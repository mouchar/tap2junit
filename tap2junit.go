package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/jstemmer/go-junit-report/formatter"
	"github.com/jstemmer/go-junit-report/parser"
	"github.com/mpontillo/tap13"
)

var (
	packageName = flag.String("package-name", "", "specify a package name")
	setExitCode = flag.Bool("set-exit-code", false, "set exit code to 1 if tests failed")
)

func ReadTap() []string {
	var lines []string
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines
}

func Convert(item *tap13.Test) *parser.Test {
	var r parser.Result = parser.PASS
	switch {
	case item.Passed:
		r = parser.PASS
	case item.Failed:
		r = parser.FAIL
	case item.Skipped:
		r = parser.SKIP
	}
	out := &parser.Test{
		Name:     item.Description,
		Duration: time.Duration(0),
		Result:   r,
	}
	return out
}

func main() {
	flag.Parse()

	lines := ReadTap()
	parsed := tap13.Parse(lines)
	report := &parser.Report{
		Packages: []parser.Package{
			parser.Package{
				Name:        *packageName,
				Duration:    time.Duration(0),
				Benchmarks:  nil,
				CoveragePct: "",
			},
		},
	}
	for _, item := range parsed.Tests {
		report.Packages[0].Tests = append(report.Packages[0].Tests, Convert(&item))
	}
	err := formatter.JUnitReportXML(report, false, "", os.Stdout)
	if err != nil {
		fmt.Printf("Error writing XML: %s\n", err)
		os.Exit(1)
	}
}
