module gitlab.com/mouchar/tap2junit

go 1.13

require (
	github.com/jstemmer/go-junit-report v0.9.1
	github.com/mpontillo/tap13 v1.0.1
)
